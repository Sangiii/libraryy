from django.urls import path, include
from rest_framework import routers
from . import views 
#from .views import EmployeePostRudView, EmployeePostAPIView

router = routers.DefaultRouter()
router.register('lib', views.BookPostView)
router.register('booktype', views.BookTypeView)
router.register('admin', views.AdminView)

urlpatterns = [
    path('',include(router.urls))
]   
