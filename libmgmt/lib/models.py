from django.conf import settings
from django.db import models
from django.urls import reverse

from rest_framework.reverse import reverse as api_reverse

# django hosts --> subdomain for reverse
class BookType(models.Model):
    types = models.CharField(max_length=50)
    

    def __str__(self):
        return self.types

class BookPost(models.Model):
    # pk aka id --> numbers
    bid         = models.IntegerField(null=True, blank=True)
    types    = models.ForeignKey(BookType, null=True, on_delete=models.CASCADE)
    bookname    = models.CharField(max_length=120, null=True, blank=True)
    author      = models.CharField(max_length=120, null=True, blank=True)
    publication = models.CharField(max_length=120, null=True, blank=True)
    published   = models.CharField(max_length=120, null=True, blank=True)
    def __str__(self):
        return self.types


class Admin(models.Model):
    
    bookpost = models.ManyToManyField(BookPost)

    def __str__(self):
        return str(self.types)

   # @property
    #def owner(self):
     #   return self.types

    # def get_absolute_url(self):
    #     return reverse("api-postings:post-rud", kwargs={'pk': self.pk}) '/api/postings/1/'
    
    #def get_api_url(self, request=None):
     #   return api_reverse("api-postings:post-rud", kwargs={'pk': self.pk}, request=request)