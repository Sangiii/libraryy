from rest_framework import serializers
from .models import Api

# Create your views here.

class ApiSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Api
		fields = {'eid', 'url', 'ename', 'bid', 'bname', 'issue', 'renew'}

#class BnameSerializer(serializers.HyperlinkedModelSerializer):
#	class Meta:
#		model = bname
#		fields = ('eid', 'url', 'ename', 'bid', 'issue', 'renew')

#class EmployeeSerializer(serializers.HyperlinkedModelSerializer):
#	class Meta:
#		model = employee 
#		fields = ('eid', 'url', 'api')


